package de.uni_passau.fim.information_extraction.simple_graphene.runner;

import de.fim.uni_passau.information_extraction.discourse_simplification.Processor;
import de.fim.uni_passau.information_extraction.discourse_simplification.output_generation.Context;
import de.fim.uni_passau.information_extraction.discourse_simplification.output_generation.Core;
import de.uni_passau.fim.information_extraction.simple_graphene.model.SimplifiedSentence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper class that handles calls to the sentence simplification library.
 *
 * Created by bernhard on 25.07.16.
 */
public class SimplificationRunner {
    private static final Logger LOG = LoggerFactory.getLogger(SimplificationRunner.class);

    /**
     * Does the sentence simplification for the given list of sentences.
     *
     * @param sentences list of sentences to simplify
     * @return list of simplified sentences
     */
    public static List<SimplifiedSentence> doSimplification(List<String> sentences) {

        LOG.debug("Starting simplification for {} sentences", sentences.size());

        Processor processor = new Processor();
        List<Core> cores = processor.process(sentences);

        List<SimplifiedSentence> simplifiedSentences = new ArrayList<>();

        LOG.debug("Preparing simplified sentences from {} Cores", cores.size());
        for (Core core : cores) {

            SimplifiedSentence simplifiedSentence = new SimplifiedSentence();

            List<String> currentCores = new ArrayList<>();
            currentCores.add(core.getText());

            List<String> currentContexts = new ArrayList<>();
            for (Context context : core.getContexts()) {
                currentContexts.add(context.getText());
            }

            simplifiedSentence.setOriginal(core.getOriginalSentence());
            simplifiedSentence.setCores(currentCores);
            simplifiedSentence.setContexts(currentContexts);

            simplifiedSentences.add(simplifiedSentence);

        }

        LOG.debug("Simplified {} sentences into {} simplified sentences", sentences.size(), simplifiedSentences.size());

        return simplifiedSentences;
    }
}
