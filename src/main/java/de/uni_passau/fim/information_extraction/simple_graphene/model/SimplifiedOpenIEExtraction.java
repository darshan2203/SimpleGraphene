package de.uni_passau.fim.information_extraction.simple_graphene.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that holds information about a relation extraction from
 * a simplified sentence.
 *
 * Created by bernhard on 19.04.16.
 */
public class SimplifiedOpenIEExtraction {

    private String original = "";

    private List<OpenIEExtraction> coreExtraction = new ArrayList<>();

    private List<OpenIEExtraction> contextExtractions = new ArrayList<>();

    public SimplifiedOpenIEExtraction() {
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public List<OpenIEExtraction> getCoresExtractions() {
        return coreExtraction;
    }

    public void setCoresExtractions(List<OpenIEExtraction> coreExtraction) {
        this.coreExtraction = coreExtraction;
    }

    public List<OpenIEExtraction> getContextExtractions() {
        return contextExtractions;
    }

    public void setContextExtractions(List<OpenIEExtraction> contextExtractions) {
        this.contextExtractions = contextExtractions;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
