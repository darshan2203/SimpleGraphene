package de.uni_passau.fim.information_extraction.simple_graphene;

import de.uni_passau.fim.information_extraction.simple_graphene.utility.FileIO;
import de.uni_passau.fim.information_extraction.simple_graphene.utility.OutputWriter;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.MutuallyExclusiveGroup;
import net.sourceforge.argparse4j.inf.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static de.uni_passau.fim.information_extraction.simple_graphene.SimpleRelationExtractionCLI.Runtypes.*;

/**
 * Command line interface as a tool to wrap Sentence Simplification and OpenIE and chain
 * the output of both tools.
 *
 * Created by bernhard on 25.07.16.
 */
public class SimpleRelationExtractionCLI {

    private static final Logger logger = LoggerFactory.getLogger(SimpleRelationExtractionCLI.class);

    public static void main(String[] args) {

        ArgumentParser parser = createParser();

        try {
            Namespace ns = parser.parseArgs(args);

            List<String> sentences = new ArrayList<>();

            if (ns.get("input_file") != null) {
                sentences.addAll(FileIO.readFile(ns.get("input_file")));
            } else {
                sentences.add(ns.get("sentence"));
            }

            OutputWriter writer = OutputWriter.create(ns.get("output_file"));

            List output = Collections.EMPTY_LIST;

            switch ((Runtypes) ns.get("type")) {
                case BOTH:
                    output = SimpleRelationExtraction.simplifyThenExtract(sentences);
                    break;
                case SIMPLIFY:
                    output = SimpleRelationExtraction.simplify(sentences);
                    break;
                case OPENIE:
                    output = SimpleRelationExtraction.extract(sentences);
                    break;
            }

            writer.write(output);

        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        } catch (IOException e) {
            logger.error("During execution, an IOException occured: {}", e.getMessage());
        }
    }

    /**
     * Creates the Argument parser for the command line arguments.
     *
     * @return ArgumentParser initialized
     */
    private static ArgumentParser createParser() {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("SimpleRelationExtraction")
                .description("Do simplification and information extraction.");

        parser.addArgument("-t", "--type")
                .type(Runtypes.class)
                .choices(BOTH, OPENIE, SIMPLIFY)
                .setDefault(BOTH);

        MutuallyExclusiveGroup inputGroup = parser.addMutuallyExclusiveGroup();

        inputGroup.addArgument("-i", "--input-file")
                .type(Arguments.fileType().acceptSystemIn().verifyCanRead())
                .help("The input file with multiple sentences to operate on.");

        inputGroup.addArgument("-s", "--sentence")
                .type(String.class)
                .help("One single sentence on which to operate on.");

        inputGroup.required(true);

        MutuallyExclusiveGroup outputGroup = parser.addMutuallyExclusiveGroup();

        outputGroup.required(true);

        outputGroup.addArgument("-o", "--output-file")
                .type(Arguments.fileType().verifyCanCreate());

        outputGroup.addArgument("-c", "--print-to-console")
                .action(Arguments.storeTrue())
                .setDefault(true);

        return parser;
    }

    enum Runtypes {
        /**
         * Do only sentence simplification.
         */
        SIMPLIFY,

        /**
         * Do only relation extraction with OpenIE.
         */
        OPENIE,

        /**
         * Do sentence simplification and then relation extraction on the simplified sentences.
         */
        BOTH
    }
}
