package de.uni_passau.fim.information_extraction.simple_graphene.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * Class that holds information about a relation extraction from OpenIE.
 *
 * Created by bernhard on 01.05.16.
 */
public class OpenIEExtraction {

    private String original;

    private String subject;

    private String predicate;

    private String object;

    private String context;

    private List<String> temporalContexts;

    private List<String> spatialContexts;

    private List<String> additionalContexts;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public List<String> getTemporalContexts() {
        return temporalContexts;
    }

    public void setTemporalContexts(List<String> temporalContexts) {
        this.temporalContexts = temporalContexts;
    }

    public List<String> getSpatialContexts() {
        return spatialContexts;
    }

    public void setSpatialContexts(List<String> spatialContexts) {
        this.spatialContexts = spatialContexts;
    }

    public List<String> getAdditionalContexts() {
        return additionalContexts;
    }

    public void setAdditionalContexts(List<String> additionalContexts) {
        this.additionalContexts = additionalContexts;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
