package de.uni_passau.fim.information_extraction.simple_graphene.runner;

import de.uni_passau.fim.information_extraction.simple_graphene.model.OpenIEExtraction;
import de.uni_passau.fim.information_extraction.simple_graphene.model.SimplifiedOpenIEExtraction;
import de.uni_passau.fim.information_extraction.simple_graphene.model.SimplifiedSentence;
import edu.knowitall.openie.*;
import edu.knowitall.tool.parse.ClearParser;
import edu.knowitall.tool.postag.ClearPostagger;
import edu.knowitall.tool.srl.ClearSrl;
import edu.knowitall.tool.tokenize.ClearTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.collection.Seq;
import scala.collection.convert.WrapAsJava$;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that wraps the call to the OpenIE library.
 *
 * Created by bernhard on 25.07.16.
 */
public class OpenIERunner {
    private final static Logger logger = LoggerFactory.getLogger(OpenIERunner.class);

    private static final OpenIE OPENIE = initializeOpenIE();

    private static OpenIE initializeOpenIE() {
        long started = System.currentTimeMillis();

        OpenIE openie =
                new OpenIE(
                        new ClearParser(
                                new ClearPostagger(
                                        new ClearTokenizer())
                        ),
                        new ClearSrl(),
                        false,
                        true);
        logger.info("OpenIE initialized, took {} seconds", (System.currentTimeMillis() - started) / 1000);

        return openie;
    }

    /**
     * Runs OpenIE on the given sentences.
     *
     * @param sentences list of sentences to run OpenIE on
     * @return list of extractions for the given sentences
     */
    public static List<OpenIEExtraction> doOpenIE(List<String> sentences) {
        List<OpenIEExtraction> out = new ArrayList<>();
        for (String s : sentences) {
            out.addAll(extract(s));
        }
        return out;
    }

    private static List<OpenIEExtraction> extract(String sentence) {

        Seq<Instance> extractions_raw = OPENIE.extract(sentence);

        List<OpenIEExtraction> extractions = new ArrayList<>();

        for (Instance instance : convertSeqToList(extractions_raw)) {
            OpenIEExtraction extraction = new OpenIEExtraction();

            extraction.setOriginal(instance.sentence());
            extraction.setSubject(instance.extraction().arg1().text());
            extraction.setPredicate(instance.extraction().rel().text());

            if (instance.extraction().context().isDefined() && !instance.extraction().context().isEmpty()) {
                extraction.setContext(instance.extraction().context().toString());
            }

            List<String> simple = new ArrayList<>();
            List<String> temporal = new ArrayList<>();
            List<String> spatial = new ArrayList<>();
            List<String> additional = new ArrayList<>();

            List<Argument> arguments = convertSeqToList(instance.extraction().arg2s());

            for (Argument argument : arguments) {
                if (argument instanceof TemporalArgument) {
                    temporal.add(argument.text());
                } else if (argument instanceof SpatialArgument) {
                    spatial.add(argument.text());
                } else if (argument instanceof SimpleArgument) {
                    simple.add(argument.text());
                } else {
                    additional.add(argument.text());
                }
            }

            if (temporal.size() > 1) {
                logger.warn("OpenIE returned multiple temporal objects: {}", temporal.size());
            }
            if (spatial.size() > 1) {
                logger.warn("OpenIE returned multiple spatial objects: {}", spatial.size());
            }
            if (additional.size() != 0) {
                logger.warn("OpenIE gave {} additional objects that are not simple, temporal or spatial", additional.size());
            }

            if (simple.size() == 0) {
                logger.info("OpenIE did not find an object in the sentence '{}'", instance.sentence());
            } else if (simple.size() > 1) {
                logger.warn("OpenIE has not returned one object but more just using the first one.\nAll are: '{}'", simple.toString());
            }

            if (simple.size() > 0) {
                extraction.setObject(simple.get(0)); // using the first one, most of the time there is only one
            }


            extraction.setTemporalContexts(temporal);
            extraction.setSpatialContexts(spatial);
            extraction.setAdditionalContexts(additional);


            extractions.add(extraction);
        }

        return extractions;
    }

    /**
     * Runs OpenIE on the given simplified sentences. The sentences must be already in simplified
     * form.
     *
     * @param sentences list of already simplified sentences
     * @return list of relation extractions for simplified sentences
     */
    public static List<SimplifiedOpenIEExtraction> doSimplifiedOpenIE(List<SimplifiedSentence> sentences) {
        List<SimplifiedOpenIEExtraction> out = new ArrayList<>();

        for (SimplifiedSentence ss : sentences) {
            SimplifiedOpenIEExtraction simplifiedExtraction = new SimplifiedOpenIEExtraction();

            simplifiedExtraction.setOriginal(ss.getOriginal());

            List<OpenIEExtraction> currentCores = new ArrayList<>();
            List<OpenIEExtraction> currentContexts = new ArrayList<>();

            for (String core : ss.getCores()) {
                currentCores.addAll(extract(core));
            }

            for (String context : ss.getContexts()) {
                currentContexts.addAll(extract(context));
            }

            simplifiedExtraction.setCoresExtractions(currentCores);
            simplifiedExtraction.setContextExtractions(currentContexts);

            out.add(simplifiedExtraction);
        }

        return out;
    }

    /**
     * Helper method to convert a scala sequence into a java list.
     *
     * @param seq the scala sequence to convert
     * @param <A> the type of elements in the scala sequence
     * @return Java list converted from the scala sequence
     */
    private static <A> List<A> convertSeqToList(Seq<A> seq) {
        return WrapAsJava$.MODULE$.seqAsJavaList(seq);
    }
}
