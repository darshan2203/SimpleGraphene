package de.uni_passau.fim.information_extraction.simple_graphene.utility;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utilities class for accessing files.
 * <p>
 * Created by bernhard on 29.07.16.
 */
public class FileIO {

    /**
     * Reads the given file and returns the lines as as list.
     *
     * @param file The text file to read
     * @return Lines of the given file
     * @throws IOException if file does not exist or any other IO error occurs
     */
    public static List<String> readFile(File file) throws IOException {
        return Files.lines(file.toPath()).collect(Collectors.toList());
    }
}
