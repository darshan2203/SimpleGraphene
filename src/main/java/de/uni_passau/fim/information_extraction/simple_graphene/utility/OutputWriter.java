package de.uni_passau.fim.information_extraction.simple_graphene.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * Created by bernhard on 25.07.16.
 */
public class OutputWriter {

    private final static Logger logger = LoggerFactory.getLogger(OutputWriter.class);

    private final static Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .serializeNulls()
            .create();

    private Writer bf;

    private OutputWriter() {
    }

    public static OutputWriter create(File outputFile) throws IOException {
        OutputWriter writer = new OutputWriter();

        if (outputFile != null) {
            writer.bf = new BufferedWriter(new FileWriter(outputFile));
            logger.debug("Created a FileWriter");
        } else {
            writer.bf = new BufferedWriter(new PrintWriter(System.out, true));
            logger.debug("Created a PrintWriter");
        }

        return writer;
    }

    private void write(String out) throws IOException {
        this.bf.write(out);
        this.bf.write("\n");  // new line at the end
        this.bf.flush();
        this.bf.close();
    }

    public void write(List<?> sentences) throws IOException {
        write(gson.toJson(sentences));
    }
}
