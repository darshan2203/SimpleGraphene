package de.uni_passau.fim.information_extraction.simple_graphene;

import de.uni_passau.fim.information_extraction.simple_graphene.model.OpenIEExtraction;
import de.uni_passau.fim.information_extraction.simple_graphene.model.SimplifiedOpenIEExtraction;
import de.uni_passau.fim.information_extraction.simple_graphene.model.SimplifiedSentence;
import de.uni_passau.fim.information_extraction.simple_graphene.runner.OpenIERunner;
import de.uni_passau.fim.information_extraction.simple_graphene.runner.SimplificationRunner;

import java.util.List;

/**
 * Class that provides wrapping methods for simplifying sentences, extracting
 * relations for given sentences or the relation extraction of simplified
 * sentences.
 *
 * Created by bernhard on 29.07.16.
 */
public class SimpleRelationExtraction {

    private SimpleRelationExtraction() {
    }

    /**
     * Simplifies the given sentences one at a time.
     *
     * @param sentences list of sentences to simplify
     * @return simplified list of sentences
     */
    public static List<SimplifiedSentence> simplify(List<String> sentences) {
        return SimplificationRunner.doSimplification(sentences);
    }

    /**
     * Simplifies the given sentences and then does relation extraction on
     * the parts of the simplified sentences.
     *
     * @param sentences list of sentences to simplify and then extract relations
     * @return list of extracted relations from simplified sentences
     */
    public static List<SimplifiedOpenIEExtraction> simplifyThenExtract(List<String> sentences) {
        return OpenIERunner.doSimplifiedOpenIE(
                SimplificationRunner.doSimplification(sentences)
        );
    }

    /**
     * Extracts relations in the given sentences.
     * Note, that one sentence can have multiple extractions or none.
     *
     * @param sentences list of sentences
     * @return list of extractions for the given sentences.
     */
    public static List<OpenIEExtraction> extract(List<String> sentences) {
        return OpenIERunner.doOpenIE(sentences);
    }
}
