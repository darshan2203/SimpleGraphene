package de.uni_passau.fim.information_extraction.simple_graphene;

import de.uni_passau.fim.information_extraction.simple_graphene.model.OpenIEExtraction;
import de.uni_passau.fim.information_extraction.simple_graphene.model.SimplifiedOpenIEExtraction;
import de.uni_passau.fim.information_extraction.simple_graphene.model.SimplifiedSentence;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bernhard on 18.08.16.
 */
public class SimpleRelationExtractionTest {

    private static final String EXAMPLE_SENTENCE = "The U.S. president Barack Obama gave his speech on Tuesday to thousands of people.";

    @Test
    public void simplify() throws Exception {
        List<String> sentences = new ArrayList<>();

        sentences.add(EXAMPLE_SENTENCE);

        List<SimplifiedSentence> simplifiedSentences = SimpleRelationExtraction.simplify(sentences);

        Assert.assertEquals(1, simplifiedSentences.size());

        SimplifiedSentence ss = simplifiedSentences.get(0);

        Assert.assertEquals(1, ss.getCores().size());
        Assert.assertEquals(1, ss.getContexts().size());

        String core = ss.getCores().get(0);
        String context = ss.getContexts().get(0);

        Assert.assertEquals(EXAMPLE_SENTENCE, ss.getOriginal());
        Assert.assertEquals("Barack Obama gave his speech on Tuesday to thousands of people .", core);
        Assert.assertEquals("Barack Obama was an U.S. president .", context);
    }

    @Test
    public void simplifyThenExtract() throws Exception {
        List<String> sentences = new ArrayList<>();
        sentences.add(EXAMPLE_SENTENCE);
        List<SimplifiedOpenIEExtraction> extractions = SimpleRelationExtraction.simplifyThenExtract(sentences);

        Assert.assertEquals(1, extractions.size());

        SimplifiedOpenIEExtraction simplifiedOpenIEExtraction;
        simplifiedOpenIEExtraction = extractions.get(0);

        Assert.assertEquals(1, simplifiedOpenIEExtraction.getCoresExtractions().size());
        Assert.assertEquals(1, simplifiedOpenIEExtraction.getContextExtractions().size());

        OpenIEExtraction ex;

        ex = simplifiedOpenIEExtraction.getCoresExtractions().get(0);
        Assert.assertEquals("Barack Obama gave his speech on Tuesday to thousands of people .", ex.getOriginal());
        Assert.assertEquals("Barack Obama", ex.getSubject());
        Assert.assertEquals("gave", ex.getPredicate());
        Assert.assertEquals("his speech", ex.getObject());
        Assert.assertNull(ex.getContext());
        Assert.assertEquals(1, ex.getTemporalContexts().size());
        Assert.assertEquals(0, ex.getSpatialContexts().size());
        Assert.assertEquals(0, ex.getAdditionalContexts().size());

        ex = simplifiedOpenIEExtraction.getContextExtractions().get(0);
        Assert.assertEquals("Barack Obama was an U.S. president .", ex.getOriginal());
        Assert.assertEquals("Barack Obama", ex.getSubject());
        Assert.assertEquals("was", ex.getPredicate());
        Assert.assertEquals("an U.S. president", ex.getObject());
        Assert.assertNull(ex.getContext());
        Assert.assertEquals(0, ex.getTemporalContexts().size());
        Assert.assertEquals(0, ex.getSpatialContexts().size());
        Assert.assertEquals(0, ex.getAdditionalContexts().size());
    }

    @Test
    public void extract() throws Exception {
        List<String> sentences = new ArrayList<>();
        sentences.add(EXAMPLE_SENTENCE);
        List<OpenIEExtraction> extractions = SimpleRelationExtraction.extract(sentences);

        Assert.assertEquals(2, extractions.size());

        OpenIEExtraction ex;

        // first extraction
        ex = extractions.get(0);

        Assert.assertEquals("The U.S. president Barack Obama gave his speech on Tuesday to thousands of people.", ex.getOriginal());
        Assert.assertEquals("The U.S. president Barack Obama", ex.getSubject());
        Assert.assertEquals("gave", ex.getPredicate());
        Assert.assertEquals("his speech", ex.getObject());
        Assert.assertNull(ex.getContext());

        Assert.assertEquals(1, ex.getTemporalContexts().size());
        Assert.assertEquals(0, ex.getSpatialContexts().size());
        Assert.assertEquals(0, ex.getAdditionalContexts().size());

        Assert.assertEquals("on Tuesday", ex.getTemporalContexts().get(0));

        // second extraction
        ex = extractions.get(1);

        Assert.assertEquals("The U.S. president Barack Obama gave his speech on Tuesday to thousands of people.", ex.getOriginal());
        Assert.assertEquals("Barack Obama", ex.getSubject());
        Assert.assertEquals("[is] president [of]", ex.getPredicate());
        Assert.assertEquals("United States", ex.getObject());
        Assert.assertNull(ex.getContext());

        Assert.assertEquals(0, ex.getTemporalContexts().size());
        Assert.assertEquals(0, ex.getSpatialContexts().size());
        Assert.assertEquals(0, ex.getAdditionalContexts().size());
    }

}